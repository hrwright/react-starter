import { combineReducers } from 'redux';

// import reducers here
import basicReducer from "./basic";

export default combineReducers({ basicReducer });
